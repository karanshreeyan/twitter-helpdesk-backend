var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var Strategy = require('passport-twitter').Strategy;
var session = require('express-session');
const cors = require("cors");
var moment = require('moment');


const http = require('http')
const socketio = require('socket.io');

import { twitterCredentials, frontEndUrl } from './config'
var Twit = require('twit')
var twitter = require('twitter');
var twit = new twitter({
    consumer_key: twitterCredentials.consumer_key,
    consumer_secret: twitterCredentials.consumer_secret,
    access_token_key: twitterCredentials.access_token,
    access_token_secret: twitterCredentials.access_token_secret
});

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

// passport.use(new Strategy({
//     consumerKey: 'ccthKQjmMYFio9snFDPxiAHox',
//     consumerSecret: 'rTBHpBQFAAE8OPctE8TpHNObm7B6lKDIUh7uGMNgkmOtsum01s',
//     callbackURL: 'http://localhost:3000/twitter/return?user_id=12'
// }, function (token, tokenSecret, profile, callback) {
//     console.log('token :>> ', token);
//     console.log('tokenSecret :>> ', tokenSecret);
//     console.log('profile :>> ', profile);
//     return callback(null, profile);
// }));

// passport.serializeUser(function (user, callback) {
//     callback(null, user);
// })

// passport.deserializeUser(function (obj, callback) {
//     callback(null, obj);
// })

const app = express();

const server = http.createServer(app)
const io = socketio(server, {
    cors: {
        origin: frontEndUrl,
        methods: ["GET", "POST"],
        allowedHeaders: ["my-custom-header"],
        credentials: true
    }
})

//knex
const knex = require('knex')(require('./config/index.js').knexConnection);
const { attachPaginate } = require('knex-paginate');
attachPaginate();
app.knexConnection = knex;
global.knexConnection = knex

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(cors());

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({ secret: 'whatever', resave: true, saveUninitialized: true }))

app.use(passport.initialize())
app.use(passport.session())

// 
let counter = 0
var clientSteam = {};
// var socketConnection;

io.on('connection', socket => {
    console.log(`Socket ${socket.id} connected.`);
    console.log('socket.handshake.query.param :>> ', socket.handshake.query.param);

    counter++
    // console.log('counter :>> ', counter);
    // if (socketConnection) socketConnection = null
    // socketConnection = socket
    if (Object.keys(clientSteam).length != 0) clientSteam.destroy();
    // setTimeout(closeStream, 1000);

    // function closeStream() {
    //     console.log('Closing stream...');
    //     clientSteam.destroy();
    // }
    // socket.on('listen-tweets', (user) => {
    // console.log('user :>> ', user);

    global.knexConnection('u_users')
        .where({ user_id: socket.handshake.query.param })
        .then(response => {

            twit.stream('statuses/filter', { track: `@${response[0].user_name}` }, function (stream) {
                clientSteam = stream
                stream.on('data', function (tweet, err) {
                    console.log(tweet.text);
                    if (tweet.in_reply_to_status_id && String(tweet.in_reply_to_status_id).length > 0) {
                        global.knexConnection('u_tweet_replies')
                            .where({ tweet_id_str: tweet.id_str })
                            .then(responseDuplicate => {
                                if (!responseDuplicate || responseDuplicate.length == 0) {
                                    global.knexConnection('u_tweet_replies')
                                        .where({ tweet_id_str: tweet.in_reply_to_status_id_str })
                                        .then(replyResponse => {
                                            let insert_obj = {

                                                tweet_id: tweet.id,
                                                tweet_id_str: tweet.id_str,
                                                tweet_text: tweet.text,
                                                tweet_user_id: tweet.user.id,
                                                tweet_user_id_str: tweet.user.id_str,
                                                tweet_user_name: tweet.user.screen_name,
                                                tweet_user_full_name: tweet.user.name,
                                                tweet_user_description: tweet.user.description,
                                                tweet_user_location: tweet.user.location,
                                                profile_image_url: tweet.user.profile_image_url,
                                                profile_image_url_https: tweet.user.profile_image_url_https,
                                                in_reply_to_status_id: tweet.in_reply_to_status_id,
                                                in_reply_to_status_id_str: tweet.in_reply_to_status_id_str,
                                                in_reply_to_user_id: tweet.in_reply_to_user_id,
                                                in_reply_to_user_id_str: tweet.in_reply_to_user_id_str,
                                                in_reply_to_screen_name: tweet.in_reply_to_screen_name,
                                                retweeted: tweet.retweeted ? 'Y' : 'N',
                                                reply_count: tweet.reply_count,
                                                retweet_count: tweet.retweet_count,
                                                tweet_timestamp: tweet.timestamp_ms ? moment.unix(tweet.timestamp_ms / 1000).format('YYYY-MM-DD HH:mm:ss') : null,
                                                created_at: moment().format("YYYY-MM-DD HH:mm:ss"),

                                            }
                                            if (replyResponse && replyResponse.length > 0) {
                                                insert_obj['main_tweet_id'] = replyResponse[0].main_tweet_id,
                                                    insert_obj['main_tweet_id_str'] = replyResponse[0].main_tweet_id_str
                                            } else {
                                                insert_obj['main_tweet_id'] = tweet.in_reply_to_status_id,
                                                    insert_obj['main_tweet_id_str'] = tweet.in_reply_to_status_id_str
                                            }

                                            global.knexConnection('u_tweet_replies')
                                                .insert(insert_obj)
                                                .then(response => {
                                                    socket.emit("sendTweetReply", tweet = {
                                                        ...tweet,
                                                        main_tweet_id: insert_obj['main_tweet_id'],
                                                        main_tweet_id_str: insert_obj['main_tweet_id_str'],
                                                        created_at: moment().format("YYYY-MM-DD HH:mm:ss")
                                                    })
                                                }).catch(error => {
                                                    console.log('error :>> ', error);
                                                })
                                        })

                                }
                            }).catch(duplicateErrorReply => {
                                console.log('duplicateErrorReply :>> ', duplicateErrorReply);
                            })

                    } else {
                        global.knexConnection('u_tweets')
                            .where({ tweet_id_str: tweet.id_str })
                            .then(responseDuplicate => {
                                if (!responseDuplicate || responseDuplicate.length == 0) {
                                    global.knexConnection('u_tweets')
                                        .insert({
                                            user_id: socket.handshake.query.param,
                                            tweet_id: tweet.id,
                                            tweet_id_str: tweet.id_str,
                                            tweet_text: tweet.text,
                                            tweet_user_id: tweet.user.id,
                                            tweet_user_id_str: tweet.user.id_str,
                                            tweet_user_name: tweet.user.screen_name,
                                            tweet_user_full_name: tweet.user.name,
                                            tweet_user_description: tweet.user.description,
                                            tweet_user_location: tweet.user.location,
                                            profile_image_url: tweet.user.profile_image_url,
                                            profile_image_url_https: tweet.user.profile_image_url_https,
                                            in_reply_to_status_id: tweet.in_reply_to_status_id,
                                            in_reply_to_status_id_str: tweet.in_reply_to_status_id_str,
                                            in_reply_to_user_id: tweet.in_reply_to_user_id,
                                            in_reply_to_user_id_str: tweet.in_reply_to_user_id_str,
                                            in_reply_to_screen_name: tweet.in_reply_to_screen_name,
                                            retweeted: tweet.retweeted ? 'Y' : 'N',
                                            reply_count: tweet.reply_count,
                                            retweet_count: tweet.retweet_count,
                                            tweet_timestamp: tweet.timestamp_ms ? moment.unix(tweet.timestamp_ms / 1000).format('YYYY-MM-DD HH:mm:ss') : null,
                                            created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
                                        })
                                        .then(response => {
                                            socket.emit("sendTweet", tweet = {
                                                ...tweet,
                                                created_at: moment().format("YYYY-MM-DD HH:mm:ss")
                                            })
                                        }).catch(error => {
                                            console.log('error :>> ', error);
                                        })
                                }
                            })
                            .catch(errorDuplicate => {
                                console.log('errorDuplicate :>> ', errorDuplicate);
                            })
                    }
                });
            });




            // var T = new Twit({
            //     consumer_key: twitterCredentials.consumer_key,
            //     consumer_secret: twitterCredentials.consumer_secret,
            //     access_token: response[0].token,
            //     access_token_secret: response[0].token_secret,
            //     timeout_ms: 60 * 1000,  // optional HTTP request timeout to apply to all requests.
            //     strictSSL: true,     // optional - requires SSL certificates to be valid.
            // })

            // var stream = T.stream('statuses/filter', { track: `@${response[0].user_name}` })

            // stream.on('tweet', function (tweet) {
            //     console.log('tweets >>', tweet)
            //     if (tweet.in_reply_to_status_id && String(tweet.in_reply_to_status_id).length > 0) {
            //         global.knexConnection('u_tweet_replies')
            //             .where({ tweet_id_str: tweet.id_str })
            //             .then(responseDuplicate => {
            //                 if (!responseDuplicate || responseDuplicate.length == 0) {
            //                     global.knexConnection('u_tweet_replies')
            //                         .where({ tweet_id_str: tweet.in_reply_to_status_id_str })
            //                         .then(replyResponse => {
            //                             let insert_obj = {

            //                                 tweet_id: tweet.id,
            //                                 tweet_id_str: tweet.id_str,
            //                                 tweet_text: tweet.text,
            //                                 tweet_user_id: tweet.user.id,
            //                                 tweet_user_id_str: tweet.user.id_str,
            //                                 tweet_user_name: tweet.user.screen_name,
            //                                 tweet_user_full_name: tweet.user.name,
            //                                 tweet_user_description: tweet.user.description,
            //                                 tweet_user_location: tweet.user.location,
            //                                 profile_image_url: tweet.user.profile_image_url,
            //                                 profile_image_url_https: tweet.user.profile_image_url_https,
            //                                 in_reply_to_status_id: tweet.in_reply_to_status_id,
            //                                 in_reply_to_status_id_str: tweet.in_reply_to_status_id_str,
            //                                 in_reply_to_user_id: tweet.in_reply_to_user_id,
            //                                 in_reply_to_user_id_str: tweet.in_reply_to_user_id_str,
            //                                 in_reply_to_screen_name: tweet.in_reply_to_screen_name,
            //                                 retweeted: tweet.retweeted ? 'Y' : 'N',
            //                                 reply_count: tweet.reply_count,
            //                                 retweet_count: tweet.retweet_count,
            //                                 tweet_timestamp: tweet.timestamp_ms ? moment.unix(tweet.timestamp_ms / 1000).format('YYYY-MM-DD HH:mm:ss') : null,
            //                                 created_at: moment().format("YYYY-MM-DD HH:mm:ss"),

            //                             }
            //                             if (replyResponse && replyResponse.length > 0) {
            //                                 insert_obj['main_tweet_id'] = replyResponse[0].main_tweet_id,
            //                                     insert_obj['main_tweet_id_str'] = replyResponse[0].main_tweet_id_str
            //                             } else {
            //                                 insert_obj['main_tweet_id'] = tweet.in_reply_to_status_id,
            //                                     insert_obj['main_tweet_id_str'] = tweet.in_reply_to_status_id_str
            //                             }

            //                             global.knexConnection('u_tweet_replies')
            //                                 .insert(insert_obj)
            //                                 .then(response => {
            //                                     socket.emit("sendTweetReply", tweet = {
            //                                         ...tweet,
            //                                         main_tweet_id: insert_obj['main_tweet_id'],
            //                                         main_tweet_id_str: insert_obj['main_tweet_id_str'],
            //                                         created_at: moment().format("YYYY-MM-DD HH:mm:ss")
            //                                     })
            //                                 }).catch(error => {
            //                                     console.log('error :>> ', error);
            //                                 })
            //                         })

            //                 }
            //             }).catch(duplicateErrorReply => {
            //                 console.log('duplicateErrorReply :>> ', duplicateErrorReply);
            //             })

            //     } else {
            //         global.knexConnection('u_tweets')
            //             .where({ tweet_id_str: tweet.id_str })
            //             .then(responseDuplicate => {
            //                 if (!responseDuplicate || responseDuplicate.length == 0) {
            //                     global.knexConnection('u_tweets')
            //                         .insert({
            //                             user_id: user.user_id,
            //                             tweet_id: tweet.id,
            //                             tweet_id_str: tweet.id_str,
            //                             tweet_text: tweet.text,
            //                             tweet_user_id: tweet.user.id,
            //                             tweet_user_id_str: tweet.user.id_str,
            //                             tweet_user_name: tweet.user.screen_name,
            //                             tweet_user_full_name: tweet.user.name,
            //                             tweet_user_description: tweet.user.description,
            //                             tweet_user_location: tweet.user.location,
            //                             profile_image_url: tweet.user.profile_image_url,
            //                             profile_image_url_https: tweet.user.profile_image_url_https,
            //                             in_reply_to_status_id: tweet.in_reply_to_status_id,
            //                             in_reply_to_status_id_str: tweet.in_reply_to_status_id_str,
            //                             in_reply_to_user_id: tweet.in_reply_to_user_id,
            //                             in_reply_to_user_id_str: tweet.in_reply_to_user_id_str,
            //                             in_reply_to_screen_name: tweet.in_reply_to_screen_name,
            //                             retweeted: tweet.retweeted ? 'Y' : 'N',
            //                             reply_count: tweet.reply_count,
            //                             retweet_count: tweet.retweet_count,
            //                             tweet_timestamp: tweet.timestamp_ms ? moment.unix(tweet.timestamp_ms / 1000).format('YYYY-MM-DD HH:mm:ss') : null,
            //                             created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
            //                         })
            //                         .then(response => {
            //                             socket.emit("sendTweet", tweet = {
            //                                 ...tweet,
            //                                 created_at: moment().format("YYYY-MM-DD HH:mm:ss")
            //                             })
            //                         }).catch(error => {
            //                             console.log('error :>> ', error);
            //                         })
            //                 }
            //             })
            //             .catch(errorDuplicate => {
            //                 console.log('errorDuplicate :>> ', errorDuplicate);
            //             })
            //     }
            // })

        })
        .catch(err => {
            console.log('err :>> ', err);
        })
    // })

    socket.on('replyTweet', (data) => {
        console.log('in reply tweet :>> ');
        global.knexConnection('u_users')
            .where({ user_id: data.user_id })
            .then(response => {
                var twitter = new Twit({
                    consumer_key: twitterCredentials.consumer_key,
                    consumer_secret: twitterCredentials.consumer_secret,
                    access_token: response[0].token,
                    access_token_secret: response[0].token_secret,
                    timeout_ms: 60 * 1000,  // optional HTTP request timeout to apply to all requests.
                    strictSSL: true,     // optional - requires SSL certificates to be valid.
                })
                console.log('data.flag :>> ', data.flag);
                let flag = data.flag
                let og_tweet = data.tweet
                let tweet_reply_text = data.tweet_reply_text
                var res = {
                    status: `@${og_tweet.tweet_user_name} ${tweet_reply_text}`,
                    in_reply_to_status_id: '' + og_tweet.tweet_id_str
                };

                twitter.post('statuses/update', res,
                    function (err, tweet, response) {
                        if (err) console.log('err :>> ', err);
                        console.log('reply tweet >>', tweet.text);
                        global.knexConnection('u_tweet_replies')
                            .where({ tweet_id_str: tweet.id_str })
                            .then(responseDuplicate => {
                                if (!responseDuplicate || responseDuplicate.length == 0) {
                                    global.knexConnection('u_tweet_replies')
                                        .insert({

                                            tweet_id: tweet.id,
                                            tweet_id_str: tweet.id_str,
                                            tweet_text: tweet.text,
                                            tweet_user_id: tweet.user.id,
                                            tweet_user_id_str: tweet.user.id_str,
                                            tweet_user_name: tweet.user.screen_name,
                                            tweet_user_full_name: tweet.user.name,
                                            tweet_user_description: tweet.user.description,
                                            tweet_user_location: tweet.user.location,
                                            profile_image_url: tweet.user.profile_image_url,
                                            profile_image_url_https: tweet.user.profile_image_url_https,
                                            in_reply_to_status_id: tweet.in_reply_to_status_id,
                                            in_reply_to_status_id_str: tweet.in_reply_to_status_id_str,
                                            in_reply_to_user_id: tweet.in_reply_to_user_id,
                                            in_reply_to_user_id_str: tweet.in_reply_to_user_id_str,
                                            in_reply_to_screen_name: tweet.in_reply_to_screen_name,
                                            retweeted: tweet.retweeted ? 'Y' : 'N',
                                            reply_count: tweet.reply_count,
                                            retweet_count: tweet.retweet_count,
                                            tweet_timestamp: tweet.timestamp_ms ? moment.unix(tweet.timestamp_ms / 1000).format('YYYY-MM-DD HH:mm:ss') : null,
                                            created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
                                            main_tweet_id: flag == 'mainTweet' ? tweet.in_reply_to_status_id : og_tweet.main_tweet_id,
                                            main_tweet_id_str: flag == 'mainTweet' ? tweet.in_reply_to_status_id_str : og_tweet.main_tweet_id_str
                                        })
                                        .then(response => {
                                            socket.emit("sendTweetReply", tweet = {
                                                ...tweet,
                                                main_tweet_id_str: flag == 'mainTweet' ? tweet.in_reply_to_status_id_str : og_tweet.main_tweet_id_str,
                                                main_tweet_id: flag == 'mainTweet' ? tweet.in_reply_to_status_id : og_tweet.main_tweet_id,
                                                created_at: moment().format("YYYY-MM-DD HH:mm:ss")
                                            })
                                        }).catch(error => {
                                            console.log('error :>> ', error);
                                        })
                                }
                            }).catch(duplicateErrorReply => {
                                console.log('duplicateErrorReply :>> ', duplicateErrorReply);
                            })
                    }
                );
            }).catch(error => {
                console.log('error in reply tweet :>> ', error);
            })


    })

    socket.on('disconnect', () => {
        console.log(`Socket ${socket.id} disconnected.`);
        counter--
        // console.log('counter :>> ', counter);
        if (Object.keys(clientSteam).length != 0) clientSteam.destroy();
        //     setTimeout(closeStream, 1000);
    });
})

app.use('/', indexRouter);
app.use('/users', usersRouter);


// app.get('/', function (req, res) {
//     res.render('index', { user: req.user })
// })

// app.get('/twitter/login', passport.authenticate('twitter'))

// app.get('/twitter/return', passport.authenticate('twitter', {
//     failureRedirect: '/'
// }), function (req, res) {
//     res.redirect('/')
// })


const PORT = 3000 || process.env.PORT;

server.listen(PORT, () => console.log(`listening on port ${PORT}`))

// module.exports = app;