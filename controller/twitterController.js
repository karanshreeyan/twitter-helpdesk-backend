var async = require('async');
var moment = require('moment');

import { twitterCredentials } from '../config'

var Twit = require('twit')


export const GetTweets = async function (req, res) {
    // console.log('req.meta :>> ', req.meta);

    let response = await req.app.knexConnection('u_tweets')
        .where({ user_id: req.meta.user_id })
        .orderBy('created_at', 'desc')

    if (response && response.length > 0) {
        async.forEachOf(response, function (single_tweet, key, callback) {
            req.app.knexConnection('u_tweet_replies')
                .where({
                    main_tweet_id_str: single_tweet.tweet_id_str
                })
                .orderBy('created_at', 'asc')
                .then(response2 => {
                    if (response2 && response2.length > 0) {
                        single_tweet['replies'] = response2
                    } else {
                        single_tweet['replies'] = []
                    }
                    callback()
                })
                .catch(error => {
                    console.log('error :>> ', error);
                    callback(error)
                })
        },
            function (eachError) {
                if (eachError) {
                    return res.json({ status: false, message: 'failed to get tweets', error: eachError })
                } else {
                    return res.json({ status: true, message: 'tweets fetched', Records: response })
                }
            })

    } else {
        return res.json({ status: true, message: 'no tweets found', Records: [] })
    }




}