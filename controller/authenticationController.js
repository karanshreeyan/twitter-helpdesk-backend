var async = require('async');
var moment = require('moment');
var bcrypt = require('bcryptjs');

module.exports.Register = async function (req, res) {
    try {

        let user_full_name = req.body.user_full_name
        let email = req.body.email
        let password = req.body.password
        let hash = password == null ? '' : bcrypt.hashSync(password, 10);

        let response1 = await req.app.knexConnection('u_users')
            .where({
                user_email: email
            })

        if (response1 && response1.length > 0) {
            return res.json({ status: false, message: 'Email already exists, please login' })
        } else {
            let response2 = await req.app.knexConnection('u_users')
                .insert({
                    user_full_name: user_full_name,
                    user_email: email,
                    password: hash,
                    user_is_active: 'Y',
                    created_by: -1,
                    created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
                })

            if (response2) {
                // return res.json({
                //     status: true, message: 'User created successfully'
                // })
                return res.redirect('/twitter/login')
            }
        }

    } catch (error) {
        return res.json({ status: false, message: 'Error while registering user', error })
    }
}

