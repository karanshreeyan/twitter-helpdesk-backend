// import * as constant from "@/app/helpers/constant";
// import * as collection from "@/app/common/collection";

// import statusTypes from "@/app/enum/statusTypes";

// import ResponseService from "@/app/services/ResponseService";
import * as securityClient from "../securityService/securityClient";

// Create a service to customer check

export const requestCheck = async (req, res, next) => {
	const { app, headers } = req;
	// console.log('headers :>> ', headers);
	// console.log('headers.authorization :>> ', headers.authorization);
	if (!headers.authorization) return res.status(403).json({ status: false, isAuthorization: true, message: 'You are not authorized' })

	// verify the token first
	const verify = securityClient.jwtVerify(headers.authorization);
	if (!verify) return res.status(403).json({ status: false, isAuthorization: true, message: 'You are not authorized' })

	const jwtData = securityClient.jwtDecode(headers.authorization);
	if (!jwtData || !jwtData['encryptToken']) return res.status(403).json({ status: false, isAuthorization: true, message: 'You are not authorized' })

	const _user_data = securityClient.decrypt(jwtData.encryptToken);
	const user_data = JSON.parse(_user_data)
	// console.log('user_data :>> ', user_data);

	if (!user_data || !user_data.user_id) return res.status(403).json({ status: false, isAuthorization: true, message: 'You are not authorized' })

	const existingClient = await req.app.knexConnection('u_users')
		.where({ user_id: user_data.user_id })
	// console.log('existingClient :>> ', existingClient);
	if (!existingClient || existingClient.length == 0) return res.status(403).json({ status: false, isAuthorization: true, message: 'You are not authorized' })

	// attach meta
	const _meta = {
		apiType: 'CLIENT',
		clientId: user_data.user_id,
		// ...user_data
		...existingClient[0]
	};

	// eslint-disable-next-line require-atomic-updates
	req.meta = { ...req.meta, ..._meta };
	// console.log('req.meta :>> ', req.meta);
	return next();
};