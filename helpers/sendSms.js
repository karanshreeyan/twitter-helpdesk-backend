const axios = require('axios');



module.exports.sendSMS = function (mobileNumber, text, callback) {
    return new Promise((resolve, reject) => {
        if (mobileNumber.length > 9 && text) {
            let url = `http://smsidea.co.in/sendsms.aspx?mobile=9599510171&pass=9599510171&senderid=CARVOC&to=${mobileNumber}&msg=${text}`
            const encodedURL = encodeURI(url);
            axios.get(encodedURL)
                .then(function (response) {
                    // handle success
                    if (response) {
                        // console.log(response);
                        return resolve({ status: true, message: 'Message Sent Successfully' })
                    }
                }).catch(error => {
                    console.log('error :>> ', error);
                    return resolve({ status: false, message: 'Failed to send Sms', error })

                })
        } else {
            return resolve({ status: false, message: 'Invalid Input' })
        }
    });


}
