const cryptoJs = require("crypto-js");
const jwt = require("jsonwebtoken");
const md5 = require("md5");

import * as constant from "../helpers/constant";



// class securityClient {

//     constructor() { }

export const jwtEncode = function (payload) {
    return new Promise((resolve, reject) => {
        // console.log('payload :>> ', payload);
        try {
            return resolve(jwt.sign(payload, constant.config.utils.JWT_SECRET, { expiresIn: constant.config.utils.JWT_TOKEN_EXPIRE }));
        } catch (exe) {

            return reject(exe)
        }
    });

}

export const jwtDecode = function (token) {
    try {
        return jwt.decode(token);
    } catch (exe) {

        return null;
    }
}

export const jwtVerify = function (token) {
    try {
        return jwt.verify(token, constant.config.utils.JWT_SECRET);
    } catch (exe) {

        return null;
    }
}


// hash(plainText) {
//     try {
//         return md5(plainText);
//     } catch (exe) {

//         return null;
//     }
// }


export const encrypt = function (plainText) {
    return new Promise((resolve, reject) => {
        // console.log('plainText :>> ', plainText);
        try {
            return resolve(cryptoJs.AES.encrypt(plainText, constant.config.utils.ENCRYPT_SALT).toString());
        } catch (exe) {

            reject(exe)
        }
    });

}

export const decrypt = function (cipherText) {
    try {
        return cryptoJs.AES.decrypt(cipherText, constant.config.utils.ENCRYPT_SALT).toString(cryptoJs.enc.Utf8);
    } catch (exe) {

        return null;
    }
}

// }


// const SecurityClient = new securityClient();
// export default SecurityClient;
