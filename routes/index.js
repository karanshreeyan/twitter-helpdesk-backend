var express = require('express');
var router = express.Router();

var authenticationController = require('../controller/authenticationController')

var passport = require('passport');
var Strategy = require('passport-twitter').Strategy;

import { twitterCredentials } from '../config'

var moment = require('moment');
var async = require('async');

import * as securityClient from "../securityService/securityClient";

const frontEndUrl = require('../config/index.js').frontEndUrl;

import { requestCheck } from '../middleware/clientAuthMiddleware'

import * as twitterController from '../controller/twitterController'

passport.use(new Strategy({
  consumerKey: twitterCredentials.consumer_key,
  consumerSecret: twitterCredentials.consumer_secret,
  callbackURL: twitterCredentials.callback_url
}, function (token, tokenSecret, profile, callback) {
  console.log('token :>> ', token);
  console.log('tokenSecret :>> ', tokenSecret);
  console.log('profile :>> ', profile);
  global.knexConnection('u_users')
    .where({ twitter_id: profile.id })
    .then(response1 => {
      if (!response1 || response1.length == 0) {
        global.knexConnection('u_users')
          .insert({
            user_name: profile.username,
            user_full_name: profile.displayName,
            twitter_id: profile.id,
            user_location: profile.location,
            token: token,
            token_secret: tokenSecret,
            created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
            user_is_active: 'Y'
          })
          .then(response2 => {

            return callback(null, profile);
          })
          .catch(err => {
            console.log('err :>> ', err);
            return callback(null, profile);
          })
      } else {
        global.knexConnection('u_users')
          .update({
            token: token,
            token_secret: tokenSecret,
            user_location: profile.location,
            user_name: profile.username,
            user_full_name: profile.displayName,
            updated_at: moment().format("YYYY-MM-DD HH:mm:ss"),
            user_is_active: 'Y'
          })
          .where({
            twitter_id: profile.id
          })
          .then(response3 => {

            return callback(null, profile);
          })
          .catch(err2 => {
            console.log('err2 :>> ', err2);
            return callback(null, profile);
          })
      }
    })
    .catch(errorFetch => {
      console.log('errorFetch :>> ', errorFetch);
      return callback(null, profile);
    })

}));

passport.serializeUser(function (user, callback) {
  callback(null, user);
})

passport.deserializeUser(function (obj, callback) {
  callback(null, obj);
})


/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { user: req.user })
});

router.post('/api/register', authenticationController.Register)


router.get('/twitter/login', (req, res, next) => passport.authenticate('twitter', {
  callbackURL: twitterCredentials.callback_url,
})(req, res, next)
)


router.get('/twitter/return', passport.authenticate('twitter', {
  failureRedirect: '/'
}), function (req, res) {
  // res.redirect('/')
  global.knexConnection('u_users')
    .where({ twitter_id: req.user.id })
    .then(async response => {
      // console.log('response[0] :>> ', response[0]);
      let jwt_obj = response[0]
      const encryptToken = await securityClient.encrypt(JSON.stringify(jwt_obj))
      // console.log('encryptToken :>> ', encryptToken);
      const token = await securityClient.jwtEncode({ encryptToken })
      // console.log('token :>> ', token);
      // console.log('frontEndUrl :>> ', frontEndUrl);

      res.redirect(301, `${frontEndUrl}/loading?token=${token}&user_id=${response[0].user_id}`)
      // return res.redirect(301, `${frontEndUrl}/home?token=${token}`)
    })
    .catch(err => {
      console.log('err :>> ', err);
      res.send("Error while logging in")
    })

})



router.get('/api/tweets', requestCheck, twitterController.GetTweets)

module.exports = router;
